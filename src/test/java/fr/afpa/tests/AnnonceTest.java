package fr.afpa.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Utilisateur;
import fr.afpa.dao.GestionAnnonceDao;
import fr.afpa.model.GestionAnnonce;
import fr.afpa.model.GestionUser;

public class AnnonceTest {
	Annonce annonce;
	Utilisateur user;
	GestionAnnonce gest;
	GestionUser gestUser;
	GestionAnnonceDao gestDAO;
	List<Annonce> listeAnnonces;
	List<Utilisateur> listeUsers;
	
	@Before
	public void setUp() throws Exception {
		gest = new GestionAnnonce();
		gestDAO = new GestionAnnonceDao();
		listeAnnonces = new ArrayList();
		listeUsers = new ArrayList();
		gestUser = new GestionUser();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAjoutAnnonce() {
		user = gestUser.checkAccess("benjira", "benjira");
		assertTrue(gest.enregistrerAnnonce("L'art de la guerre", "lycée", "11112222", "2003", "J'aime Lire", 19.0, 10, 10.0, user));
		
	}
	
	@Test
	public void testModifAnnonce() {
		user = gestUser.checkAccess("benjira", "benjira");
		listeAnnonces = gest.recupererListeAnnonces(user);
		int id = listeAnnonces.get(0).getId();
		gest.modifierAnnonce(id,"L'art de la guerre", "lycée", "11112222", "J'aime Lire","2007", 19.0, 10, 10.0, user);
		assertTrue(gestDAO.recupAnnonceDAO(id).getDateEdition().equals("2007"));
	}
	
	@Test
	public void testArchiveAnnonce() {
		user = gestUser.checkAccess("benjira", "benjira");
		listeAnnonces = gest.recupererListeAnnonces(user);
		int id = listeAnnonces.get(0).getId();
		gest.archiverAnnonce(id);
		listeAnnonces = gest.recupererListeAnnonces(user);
		assertFalse(listeAnnonces.get(0).isVisible());
	}
	
	@Test
	public void testSuppressionAnnonce() {
		user = gestUser.checkAccess("benjira", "benjira");
		listeAnnonces = gest.recupererListeAnnonces(user);
		int id = listeAnnonces.get(0).getId();
		gest.supprimerAnnonce(id);
		listeAnnonces = gest.recupererListeAnnonces(user);
		assertTrue(listeAnnonces.isEmpty());
	}
	
	

}
