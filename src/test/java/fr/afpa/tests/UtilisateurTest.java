package fr.afpa.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.afpa.beans.Utilisateur;
import fr.afpa.dao.GestionUserDao;
import fr.afpa.model.GestionUser;

public class UtilisateurTest {
	Utilisateur user;
	GestionUser gest;
	GestionUserDao gestDAO;
	List<Utilisateur> liste;
	List<Utilisateur> liste2;
	
	
	
	@Before
	public void setUp() throws Exception {
		user = new Utilisateur("BENJIRA", "Mohammed", "IqraShop", "rue des Postes", 59000, "Lille", "mbenjira@gmail.com", "0612345678", "benjira", "benjira");
		gest = new GestionUser();
		gestDAO = new GestionUserDao();
		liste = new ArrayList();
		liste2 = new ArrayList();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAjoutUtilisateur() {
		gest.enregistrerUser("BENJIRA", "Mohammed", "IqraShop", "rue des Postes", 59000, "Lille", "mbenjira@gmail.com", "0612345678", "benjira", "benjira");
		assertFalse(gestDAO.recupListeUsersDao().isEmpty());
	}
	
	
	@Test
	public void testLogin() {
		user = gestDAO.checkAccessDAO("benjira", "benjira");
		assertNotNull(user);
	}
	
	@Test
	public void testMuteUtilisateur() {
		liste = gestDAO.recupListeUsersDao();
		int id = liste.get(1).getId_user();
		gest.desactiverUser(id);
		liste2 = gestDAO.recupListeUsersDao();
		assertFalse(liste2.get(1).isActif());
		
	}
	
	
	

}
