<%@page import="fr.afpa.technics.PathFile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Authentification</title>
        <link rel="stylesheet" href="style.css" />
    </head>
    <body>
        <h1 id="welcomeTitle">Bienvenue sur Search'N Read !</h1>
    
    	<div id="menuLogin">
    	<h1>Merci de bien vouloir vous connecter</h1>
    	
    	<p style= "color: red;"> ${isConnected eq "false" ? "Vos identifiants sont erronées, veuillez réessayer." : "" } </p>
        <form method="post" action="Login">
            <input id='txtLogin' name='txtLogin' type='text' placeholder="Login" required/> <br/>
            <input name='txtPassword' type='password' placeholder="Mot de passe" required/> <br/>
            <br/>
            <input name='btnConnect' type='submit' value='Se connecter'/> <br/>
        </form>  
        <p> Pas encore de compte ? <a href='creation_compte.jsp' >S'enregistrer</a> </p>
        </div>   
    </body>
</html>
