<%@page import="fr.afpa.beans.Annonce"%>
<%@page import="java.util.List"%>
<%@page import="fr.afpa.beans.Utilisateur"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Visualiser vos annonces</title>
<link rel="stylesheet" href="style.css" />
</head>
<body>
<h1>Liste de vos annonces :</h1>

<c:forEach items="${liste}" var="element">

			
			<section id="displayListeAnnonce">
			<p>--------------------------------------------------------------------------------------------------------------------------</p>
			
			<div id=image>
			<img src="images/imageLib.jpg" alt="image du livre" />
			</div>
			
			<p>Titre du livre : <c:out value="${element['titre'] }"></c:out></p>
  			
  			<p>Code ISNB : <c:out value="${element['isbn'] }"></c:out>  </p>
           
            <p>Niveau Scolaire : <c:out value="${element['niveauScolaire'] }"></c:out> </p>
           
            <p>Maison d'edition : <c:out value="${element['maisonEdition'] }"></c:out></p>
           
            <p>Date d'edition : <c:out value="${element['dateEdition'] }"></c:out></p>
           
            <p>Prix unitaire : <c:out value="${element['prixUnit'] }"></c:out></p>
            
            <p>Quantit� : <c:out value="${element['quantite'] }"></c:out></p>
            
            <p>Remise (en %) : <c:out value="${element['remise'] }"></c:out></p>
          <div id="boutons2"> 
           <input id='modif' name='modif' type='button' onclick= "window.location.href='ModifAnnonce?annonce=<c:out value="${element['id'] }"></c:out>';" value="Modifier l'annonce"/>
			<input id='delete' name='delete' type='button' onclick="window.location.href='SupprAnnonceUser?idAnnonce=<c:out value="${element['id'] }"></c:out>';" value="Supprimer l'annonce"/>
           </div>  
           </c:forEach>
            
            <p>Vous pouvez retouner au menu principal <a href='RedirectionEspaceMembre'>en cliquant ici !</a></p>
            </section>
</body>
</html>