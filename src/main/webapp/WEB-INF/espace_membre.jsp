<%@page import="fr.afpa.beans.Utilisateur"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Espace membre</title>
<link rel="stylesheet" href="style.css" />
</head>
<body>


<h1>Bienvenue, ${sessionScope['user'].nom } ${sessionScope['user'].prenom } </h1>

<h2>Que souhaitez-vous faire ?</h2>
<div id="menu_membre">
<input id='post' name='post' type='button' onclick= "window.location.href='RedirectionCreationAnnonce';" value='Poster une annonce'/>
<input id='search' name='search' type='button' onclick="window.location.href='RedirectionSearchAnnonce';" value='Rechercher une annonce'/>
<input id='display' name='display' type='button' onclick= "window.location.href='AfficherAnnonceUser';" value='Afficher mes annonces'/>
<input id='disconnect' name='disconnect' type='button' onclick= "window.location.href= '/';" value='Me déconnecter'/>
</div>
</body>
</html>