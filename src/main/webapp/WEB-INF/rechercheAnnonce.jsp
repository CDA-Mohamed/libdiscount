<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Recherche d'annonces</title>
<link rel="stylesheet" href="style.css" />
</head>
<body>

<h1> Bienvenue dans la recherche d'annonces : </h1>

<form id="formRechercheAnnonce" method="post" action="DisplaySearchAnnonce">
  <p>Vous souhaitez rechercher une annonce par :</p>
	
	<label for="type1">Titre</label>
    <input type="radio" id="type1" name="type" value="titre">
    
	<label for="type2">Niveau Scolaire</label>
    <input type="radio" id="type2" name="type" value="niveau">
    
	<label for="type3">Ville</label>
    <input type="radio" id="type3" name="type" value="ville">
    
<section>
    <label for="type3">Votre recherche : </label>
    <input type="text" id="choix" name="choix" >

    <button id="validerRecherche" type="submit">Valider</button>
</section>
</form>

</body>
</html>