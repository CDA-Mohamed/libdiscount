<%@page import="fr.afpa.beans.Annonce"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Modifier une annonce</title>
<link rel="stylesheet" href="style.css" />
</head>
<body>


<h1>Veuillez modifier les champs souhait�s : </h1>

 			<form method="post" action="SaveModifAnnonce?idAnnonce=${annonceModif.id }">
  			<label for="titre" >Titre du livre : </label>
  			<input id='titre' name='titre' type='text' value=${annonceModif.titre } required/> <br/>
  			<label for="isbn" >Code ISNB : </label>
            <input id='isbn' name='isbn' type='text' value=${annonceModif.isbn } required/> <br/>
            <label for="niveau" >Niveau Scolaire : </label>
            <input id='niveau' name='niveau' type='text' value=${annonceModif.niveauScolaire } required/> <br/>
            <label for="maisonEd" >Maison d'edition : </label>
            <input id='maisonEd' name='maisonEd' type='text' value=${annonceModif.maisonEdition }  required/> <br/>
            <label for="dateEd" >Date d'edition : </label>
            <input id='dateEd' name='dateEd' type='text' value=${annonceModif.dateEdition } required /> <br/>
            <label for="prix" >Prix unitaire : </label>
            <input id= 'prix' name='prix' type='text' value=${annonceModif.prixUnit } required /> <br/>
            <label for="quantite" >Quantit� : </label>
            <input id='quantite' name='quantite' type='text' value=${annonceModif.quantite } required /> <br/>
            <label for="remise" >Remise (en %) : </label>
            <input name='remise' type='text' value=${annonceModif.remise } required/> <br/>
            <br/>
            <div id='boutons4'>
            <input id='btnConnect3' type='submit' value='Valider'/> <br/>
            <input id='btnReturn3' type='button' onclick= "window.location.href= 'RedirectionEspaceMembre';" value='Annuler'/> <br/>
            </div>
        </form>  
</body>
</html>