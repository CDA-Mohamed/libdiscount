<%@page import="fr.afpa.technics.PathFile"%>
<%@page import="java.util.ArrayList"%>
<%@page import="fr.afpa.beans.Annonce"%>
<%@page import="fr.afpa.beans.Utilisateur"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Annonces publi�es</title>
<link rel="stylesheet" href="style.css" />
</head>
<body>
<h1>Liste des annonces publi�es :</h1>

<section id="displayAnnoncesAdmin">
<c:forEach items="${listeUsers}" var="user">
	<c:forEach items="${user.listeAnnonces}" var="annonce">
	
			<p>--------------------------------------------------------------------------------------------------------------------------</p>
			
			<div id=image>
			<img src="images/imageLib.jpg" alt="image du livre" />
			</div>
			
			<p>Nom/Pr�nom de l'auteur : <c:out value="${user['nom'] }"></c:out> <c:out value="${user['prenom'] }"></c:out> </p>
			
			<p>Utilisateur actif : <c:out value="${user['actif'] ? 'Oui' : 'Non' }"></c:out> </p>
			
			<p> Annonce visible : <c:out value="${annonce['visible'] ? 'Oui' : 'Non' }"></c:out> </p>
			
			<p>Librairie : <c:out value="${user['nomLibrairie'] }"></c:out> </p>
			
			<p>Adresse : <c:out value="${user['voie'] }"></c:out> <c:out value="${user['codePostal'] }"></c:out> <c:out value="${user['ville'] }"></c:out></p>
			
			<p>Titre du livre : <c:out value="${annonce['titre'] }"></c:out></p>
  			
  			<p>Code ISNB : <c:out value="${annonce['isbn'] }"></c:out>  </p>
           
            <p>Niveau Scolaire : <c:out value="${annonce['niveauScolaire'] }"></c:out> </p>
           
            <p>Maison d'edition : <c:out value="${annonce['maisonEdition'] }"></c:out></p>
           
            <p>Date d'edition : <c:out value="${annonce['dateEdition'] }"></c:out></p>
           
            <p>Prix unitaire : <c:out value="${annonce['prixUnit'] }"></c:out></p>
            
            <p>Quantit� : <c:out value="${annonce['quantite'] }"></c:out></p>
            
            <p>Remise (en %) : <c:out value="${annonce['remise'] }"></c:out></p>
            
            <div id="boutons3">
           <input id='deleteAnnonceAdmin' name='deleteAnnonceAdmin' type='button' onclick="window.location.href='SupprAnnonceAdmin?idAnnonce=<c:out value="${annonce['id'] }"></c:out>';" value="Supprimer l'annonce"/>
			<input id='deleteUser' name='deleteUser' type='button' onclick="window.location.href='MuteUser?idUser=<c:out value="${user['id_user'] }"></c:out>';" value="Desactiver l'utilisateur"/>
            </div>
    </c:forEach>
 </c:forEach>
            
            <p>Vous pouvez retouner au menu principal <a href="RedirectionEspaceAdmin">en cliquant ici !</a></p>
            </section>
</body>
</html>