<%@page import="fr.afpa.beans.Annonce"%>
<%@page import="java.util.List"%>
<%@page import="fr.afpa.technics.PathFile"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Liste d'annonces trouv�es</title>
<link rel="stylesheet" href="style.css" />
</head>
<body>
<h1>Liste des annonces publi�es :</h1>

<c:forEach items="${listeAnnonces}" var="element">
			
			<section id="affichageRechercheAnnonce">
			<p>--------------------------------------------------------------------------------------------------------------------------</p>
			
			<div id=image>
			<img src="images/imageLib.jpg" alt="image du livre" />
			</div>
			
			<p>Titre du livre : <c:out value="${element['titre'] }"></c:out></p>
  			
  			<p>Code ISNB : <c:out value="${element['isbn'] }"></c:out>  </p>
           
            <p>Niveau Scolaire : <c:out value="${element['niveauScolaire'] }"></c:out> </p>
           
            <p>Maison d'edition : <c:out value="${element['maisonEdition'] }"></c:out></p>
           
            <p>Date d'edition : <c:out value="${element['dateEdition'] }"></c:out></p>
           
            <p>Prix unitaire : <c:out value="${element['prixUnit'] }"></c:out></p>
            
            <p>Quantit� : <c:out value="${element['quantite'] }"></c:out></p>
            
            <p>Remise (en %) : <c:out value="${element['remise'] }"></c:out></p>
            
            <div id="achat">
            <input id='btnAchat' name='achat' type='button' onclick="#" value="Acheter"/>
            </div>
            </c:forEach>
            
            <p>Vous pouvez retouner au menu principal <a href="RedirectionEspaceMembre">en cliquant ici !</a></p>
            </section>
</body>
</html>