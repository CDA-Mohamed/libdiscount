<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Publier une annonce</title>
<link rel="stylesheet" href="style.css" />
</head>

<body>
<h1>Veuillez renseigner les champs suivants : </h1>

 			<form id="formCreationAnnonce" method="post" action="CreationAnnonce">
  			<label for="titre" >Titre du livre :    </label>
  			<input id='titre' name='titre' type='text' required/> <br/>
  			<label for="isbn" >Code ISNB :         </label>
            <input id='isbn' name='isbn' type='text'  required/> <br/>
            <label for="niveau" >Niveau Scolaire :   </label>
            <input id='niveau' name='niveau' type='text'  required/> <br/>
            <label for="maisonEd" >Maison d'edition :  </label>
            <input id='maisonEd' name='maisonEd' type='text' required/> <br/>
            <label for="dateEd" >Date d'edition :    </label>
            <input id='dateEd' name='dateEd' type='text' required /> <br/>
            <label for="prix" >Prix unitaire :     </label>
            <input id= 'prix' name='prix' type='text' required /> <br/>
            <label for="quantite" >Quantit� :          </label>
            <input id='quantite' name='quantite' type='text' required /> <br/>
            <label for="remise" >Remise (en %) :     </label>
            <input id='remise' name='remise' type='text' required/> <br/>
            <br/>
            <section id="boutons">
            <input id='btnConnect' name='btnConnect' type='submit' value='Valider'/> <br/>
            <input id='btnReturn' name='btnReturn' type='button' onclick= "window.location.href= 'RedirectionEspaceMembre';" value='Annuler'/> <br/>
            </section>
        </form>  
</body>
</html>