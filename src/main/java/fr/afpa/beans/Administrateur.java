package fr.afpa.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@NamedQuery(name ="recupererIDAdmin", query = "FROM Administrateur a where a.login =:login and a.mdp =: mdp")

@Entity

public class Administrateur {
	@Id
	private int id;
	protected String login;
	protected String mdp;
	
	

}
