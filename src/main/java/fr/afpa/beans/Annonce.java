package fr.afpa.beans;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString


@NamedQuery(name = "afficherAnnonces", query = "FROM Utilisateur u where u.id_user =:id ")
@NamedQuery(name= "displayAllAnnonces", query= "FROM Annonce a")
@NamedQuery(name = "recupIdAnnonce", query = "FROM Annonce a where a.id =:id ")

@Entity
public class Annonce {
	
	

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;
	private String titre;
	private String niveauScolaire;
	private String isbn;
	private String dateEdition;
	private String maisonEdition;
	private double prixUnit;
	private int quantite;
	private double remise;
	private boolean visible;
	
	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinColumn(name = "noUser")
	private Utilisateur user;


	public Annonce(String titre, String niveau, String isbn, String dateEdition, String maisonEdition, double prix,
		int quantite, double remise, Utilisateur user) {
	
		this.titre = titre;
		this.niveauScolaire = niveau;
		this.isbn = isbn;
		this.dateEdition = dateEdition;
		this.maisonEdition = maisonEdition;
		this.prixUnit = prix;
		this.quantite = quantite;
		this.remise = remise;
		this.user = user;
		this.visible = true;
	}
	
	public Annonce(int id,String titre, String niveau, String isbn, String dateEdition, String maisonEdition, double prix,
			int quantite, double remise, Utilisateur user) {
		
			this.id = id;
			this.titre = titre;
			this.niveauScolaire = niveau;
			this.isbn = isbn;
			this.dateEdition = dateEdition;
			this.maisonEdition = maisonEdition;
			this.prixUnit = prix;
			this.quantite = quantite;
			this.remise = remise;
			this.user = user;
			this.visible = true;
		}

}
