package fr.afpa.beans;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString


@NamedQuery(name = "recupererID", query = "FROM Utilisateur u where u.login =:login and u.mdp =: mdp")
@NamedQuery(name = "recupererIdUser", query = "FROM Utilisateur u where u.id_user =: id")
@NamedQuery(name = "recupererUsers", query = "FROM Utilisateur u")

@Entity
public class Utilisateur {
	
	
	@Id 
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name="noUser")
	private int id_user;
	@Column(name = "login", unique=true)
	private String login;
	private String mdp;
	private String nom;
	private String prenom;
	private String nomLibrairie;
	private String voie;
	private int codePostal;
	private String ville;
	private String email;
	private String tel;
	private boolean actif;
	
	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
	private List <Annonce> listeAnnonces;
	
	public Utilisateur(String nom, String prenom, String nomLibrairie, String voie, int codePostal,String ville, String email, String tel,
			String login, String mdp) {
		
		this.nom = nom;
		this.prenom = prenom;
		this.nomLibrairie = nomLibrairie;
		this.voie = voie;
		this.codePostal = codePostal;
		this.ville = ville;
		this.email = email;
		this.tel = tel;
		this.login = login;
		this.mdp = mdp;
		this.actif = true;
	}

	public Utilisateur(String login, String password) {
		this.login = login;
		this.mdp = password;
	}

}
