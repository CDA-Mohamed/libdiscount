package fr.afpa.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Utilisateur;
import fr.afpa.session.HibernateUtils;

public class GestionAnnonceDao {

	
	
	/**
	 * Méthode permettant d'enregistrer une annonce en base de données
	 * @param annonce : prend en paramètre un objet de type Annonce
	 * @return : retourne un booléen afin de confirmer si l'enregistrement a bien été réalisé ou non
	 */
	public boolean enregistrerAnnonceDao(Annonce annonce) {
		
		Session session = null;
		try {
			session = HibernateUtils.getSession();			   
			Transaction tx = session.beginTransaction();		
			session.save(annonce);
			tx.commit();
			
			return true;
			
		} catch (Exception e) {
			
			return false;
		
			}
		finally {
			if (session != null) {
				session.close();
			}
		}
		}

	
	/**
	 * Méthode qui permet de recupérer la liste d'annonces d'un utilisateur à l'aide de son ID
	 * @param user : prend en paramètre un objet de type Utilisateur 
	 * @return : retourne la liste si celle-ci a bien été récupéré ou un null sinon
	 */
	public List<Annonce> recupererListeAnnoncesDAO(Utilisateur user) {
	
		List <Annonce> liste = new ArrayList();
		Utilisateur u = new Utilisateur();
		
		Session session = null;
		try {
			session = HibernateUtils.getSession();	
			Query query = session.getNamedQuery("afficherAnnonces");
			query.setParameter("id", user.getId_user());		
			u = (Utilisateur)query.getSingleResult();
			liste = u.getListeAnnonces();
			
			return liste;
			}
			catch (Exception e) {
				return null;
			}
		finally {
			if (session != null) {
				session.close();
			}
		}

	}

	/**
	 *  Méthode permettant de supprimer une annonce
	 * @param idAnnonce : prend en paramètre l'id de l'annonce
	 * @return : retourne true si l'annonce a bien été supprimée ou false dans le cas contraire
	 */
	public boolean supprimerAnnonceDAO(int idAnnonce) {
		Annonce a = new Annonce();
		
		Session session = null;
		try {
			session = HibernateUtils.getSession();	
			Transaction tx = session.beginTransaction();
			Query query = session.getNamedQuery("recupIdAnnonce");
			query.setParameter("id", idAnnonce);		
			a = (Annonce)query.getSingleResult();
			session.delete(a);
			tx.commit();
			
			return true;
			}
			catch (Exception e) {
				return false;
			}
		finally {
			if (session != null) {
				session.close();
			}
		}
	}

	/**
	 * Recupère une annonce à l'aide de son identifiant
	 * @param idAnnonce : identifiant de l'annonce
	 * @return : soit l'annonce soit un null
	 */
	public Annonce recupAnnonceDAO(int idAnnonce) {
		Annonce a = new Annonce();
		Session session = null;
		try {
			session = HibernateUtils.getSession();		
			Query query = session.getNamedQuery("recupIdAnnonce");
			query.setParameter("id", idAnnonce);		
			a = (Annonce)query.getSingleResult();			
			
			return a;
			}
		
			catch (Exception e) {
				return null;
			}
		finally {
			if (session != null) {
				session.close();
			}
		}
	}

	/**
	 * Modifie une annonce 
	 * @param annonce : prend en parametre une annonce
	 * @return : true si l'annonce a bien été modifié ou false si non.
	 */
	public boolean modifierAnnonceDao(Annonce annonce) {
		
		Session session = null;
		try {
			session = HibernateUtils.getSession();	
			Transaction tx = session.beginTransaction();
			session.update(annonce);
			tx.commit();
			
			return true;
			}
		
			catch (Exception e) {
				return false;
			}
		finally {
			if (session != null) {
				session.close();
			}
		}
	}


	/**
	 *  Méthode permettant d'archiver une annonce
	 * @param idAnnonce : prend en paramètre l'id de l'annonce
	 * @return : retourne true si l'annonce a bien été supprimée ou false dans le cas contraire
	 */
	public boolean archiverAnnonceDAO(int idAnnonce) {
		Annonce a = new Annonce();
		Session session = null;
		try {
			session = HibernateUtils.getSession();	
			Transaction tx = session.beginTransaction();
			Query query = session.getNamedQuery("recupIdAnnonce");
			query.setParameter("id", idAnnonce);		
			a = (Annonce)query.getSingleResult();
			a.setVisible(false);
			session.update(a);
			tx.commit();
			return true;
			}
			catch (Exception e) {
				return false;
			}
		finally {
			if (session != null) {
				session.close();
			}
		}
	}

	
}
