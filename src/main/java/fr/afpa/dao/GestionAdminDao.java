package fr.afpa.dao;

import javax.persistence.Query;

import org.hibernate.Session;

import fr.afpa.beans.Administrateur;
import fr.afpa.beans.Utilisateur;
import fr.afpa.session.HibernateUtils;

public class GestionAdminDao {

	
	/**
	 * Cherche un admin ayant les paramètres en BDD
	 * @param login
	 * @param password
	 * @return : l'admin si il a été trouvé ou null sinon.
	 */
	public Administrateur checkAccessDAO(String login, String password) {
		Administrateur admin = new Administrateur();
		
			Session session = null;
		try {
			session = HibernateUtils.getSession();			   
			Query query = session.getNamedQuery("recupererIDAdmin");
			query.setParameter("login", login);
			query.setParameter("mdp", password);
			admin = (Administrateur) query.getSingleResult();
			
		
			return admin;
			}
			catch (Exception e) {
				return null;
			}
		finally {
			if (session != null) {
				session.close();
			}
		}

		}

}
