package fr.afpa.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Utilisateur;
import fr.afpa.session.HibernateUtils;

public class GestionUserDao {

	
	/**
	 * Enregistre un utilisateur en base de données
	 * @param user : utilisateur concerné
	 * @return true si l'enregistrement a été effectué et false sinon.
	 */
	public boolean enregistrerUserDao(Utilisateur user) {
		
		Session session = null;
		try {
			session = HibernateUtils.getSession();			   
			Transaction tx = session.beginTransaction();		
			session.save(user);
			tx.commit();
			
			
			return true;
			
		} catch (Exception e) {
			
			return false;
		
		}
		finally {
			if (session != null) {
				session.close();
			}
		}
		
		
	}

	
	/**
	 * Cherche un utilisateur ayant les paramètres en BDD
	 * @param login
	 * @param password
	 * @return : l'utilisateur si il a été trouvé ou null sinon.
	 */
	public Utilisateur checkAccessDAO(String login, String password) {
		Utilisateur user = new Utilisateur();
		Session session = null;
		try {
		    session = HibernateUtils.getSession();			   
			Query query = session.getNamedQuery("recupererID");
			query.setParameter("login", login);
			query.setParameter("mdp", password);
			user = (Utilisateur)query.getSingleResult();
			
		
			return user;
			}
			catch (Exception e) {
				return null;
			}
		finally {
			if (session != null) {
				session.close();
			}
		}

		}

	
	/**
	 * Récupère la liste d'utilisateurs en BDD
	 * @return liste ou null
	 */
	public List<Utilisateur> recupListeUsersDao() {
		List <Utilisateur> liste = new ArrayList();
		Session session = null;
		try {
			session = HibernateUtils.getSession();			   
			Query query = session.getNamedQuery("recupererUsers");
			liste = (List<Utilisateur>)query.getResultList();
			
		
			return liste;
			}
			catch (Exception e) {
				return null;
			}
		finally {
			if (session != null) {
				session.close();
			}
		}
	}

	
	/**
	 * Désactive un utilisateur a l'aide de son id
	 * @param idUser
	 * @return true si l'utilisateur a été desactivé ou false sinon.
	 */
	public boolean desactiverUsersDao(int idUser) {
		
		Utilisateur user = new Utilisateur();
		List<Annonce> liste = new ArrayList();
		Session session = null;
		try {
			session = HibernateUtils.getSession();	
			Transaction tx = session.beginTransaction();
			Query query = session.getNamedQuery("recupererIdUser");
			query.setParameter("id", idUser);
			user = (Utilisateur)query.getSingleResult();
			user.setActif(false);
			liste = user.getListeAnnonces();
			for (Annonce annonce : liste) {
				annonce.setVisible(false);
			}
			session.update(user);
			tx.commit();
			
		
			return true;
			}
			catch (Exception e) {
				return false;
			}
		finally {
			if (session != null) {
				session.close();
			}
		}
	}

}