package fr.afpa.technics;

public interface PathFile {
	
	public final String ESPACE_MEMBRE = "/WEB-INF/espace_membre.jsp";
	public final String CONFIRM_COMPTE = "/WEB-INF/confirmationCreationCompte.html";
	public final String ERROR_COMPTE = "/WEB-INF/erreurCreationCompte.html";
	public final String CREATION_ANNONCE = "/WEB-INF/creationAnnonce.jsp";
	public final String CONFIRM_ANNONCE = "/WEB-INF/confirmationCreationAnnonce.html";
	public final String ERROR_ANNONCE = "/WEB-INF/erreurCreationAnnonce.html";
	public final String DISPLAY_ANNONCE = "/WEB-INF/DisplayUserAnnonce.jsp";
	public final String EMPTY_ANNONCE = "/WEB-INF/emptyAnnonce.html";
	public final String LOG_ADMIN = "/WEB-INF/authAdmin.jsp";
	public final String ESPACE_ADMIN = "/WEB-INF/espace_admin.jsp";
	public final String DISPLAY_ANNONCE_ADMIN = "/WEB-INF/DisplayAdminAnnonce.jsp";
	public final String CONFIRM_SUPPR_ANNONCE = "/WEB-INF/confirmationSuppressionAnnonce.html";
	public final String MODIF_ANNONCE = "/WEB-INF/modifierAnnonce.jsp";
	public final String ERROR_GEN = "/WEB-INF/erreur_general.html";
	public final String CONFIRM_MUTE_USER = "/WEB-INF/confirmationMuteUser.html";
	public final String CONFIRM_MODIF_ANNONCE = "/WEB-INF/confirmationModifAnnonce.jsp";
	public final String SEARCH_ANNONCE = "/WEB-INF/rechercheAnnonce.jsp";
	public final String EMPTY_SEARCH = "/WEB-INF/emptySearchAnnonce.html";
	public final String DISPLAY_SEARCH = "/WEB-INF/DisplaySearchAnnonce.jsp";
	public final String CONFIRM_ARCHIV_ANNONCE = "/WEB-INF/confirmationArchivageAnnonce.html";
	

}
