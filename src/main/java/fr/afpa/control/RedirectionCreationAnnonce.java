package fr.afpa.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.technics.PathFile;

/**
 * Servlet implementation class RedirectionCreationAnnonce
 */
public class RedirectionCreationAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
    public RedirectionCreationAnnonce() {
        super();
        
    }

	/**
	 * Redirige vers la vue de creation d'annonce.
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher(PathFile.CREATION_ANNONCE).forward(request, response);
	}

}
