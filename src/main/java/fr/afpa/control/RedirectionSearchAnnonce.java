package fr.afpa.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.technics.PathFile;

/**
 * Servlet implementation class RedirectionSearchAnnonce
 */
public class RedirectionSearchAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public RedirectionSearchAnnonce() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    /**
	 * Redirige vers la vue de recherche d'annonce.
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher(PathFile.SEARCH_ANNONCE).forward(request, response);
	}


}
