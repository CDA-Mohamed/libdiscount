package fr.afpa.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;

import fr.afpa.beans.Utilisateur;
import fr.afpa.model.GestionAnnonce;
import fr.afpa.technics.PathFile;

/**
 * Servlet implementation class CreationAnnonce
 */
public class CreationAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreationAnnonce() {
        super();
        // TODO Auto-generated constructor stub
    }

	
    /**
     * Recupere les informations du formulaire de création d'annonce et renvoie à la vue correspondante (confirmation ou erreur).
     */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String titre = request.getParameter("titre");
		String isbn = request.getParameter("isbn");
		String niveau = request.getParameter("niveau");
		String maisonEd = request.getParameter("maisonEd");
		String dateEd = request.getParameter("dateEd");
		double prix = Double.parseDouble(request.getParameter("prix"));
		int quantite = Integer.parseInt(request.getParameter("quantite"));
		double remise =  Double.parseDouble(request.getParameter("remise"));
		
		
		
		Utilisateur user = (Utilisateur) request.getSession().getAttribute("user");
		
		GestionAnnonce gest = new GestionAnnonce();
		
		if (gest.enregistrerAnnonce(titre, niveau, isbn, maisonEd, dateEd, prix, quantite, remise, user)) {
			
			
			
			// Renvoyez vers une page de confirmation d'enregistrement
			request.getRequestDispatcher(PathFile.CONFIRM_ANNONCE).forward(request, response);
			}
		
		else {
			// Renvoyez vers une page d'erreur avec champs manquants 
			request.getRequestDispatcher(PathFile.ERROR_ANNONCE).forward(request, response);
			}
	}

}
