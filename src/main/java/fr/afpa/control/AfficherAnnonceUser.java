package fr.afpa.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Utilisateur;
import fr.afpa.model.GestionAnnonce;
import fr.afpa.model.GestionUser;
import fr.afpa.technics.PathFile;

/**
 * Servlet implementation class AfficherAnnonceUser
 */
public class AfficherAnnonceUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public AfficherAnnonceUser() {
        super();
      
    }

	/**
	 * Recupere la liste d'annonces d'un utilisateur et la renvoie vers la vue correspondante en fonction de si celle-ci contient des annonces ou non
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		GestionAnnonce gest = new GestionAnnonce();
		
		Utilisateur user = (Utilisateur) request.getSession().getAttribute("user");
		
        List<Annonce> listeAllAnnonces = gest.recupererListeAnnonces(user);
        
        List<Annonce> liste = new ArrayList();
        
        for (Annonce annonce : listeAllAnnonces) {
        	if (annonce.isVisible()) {
        		liste.add(annonce);
        	}
			
		}
                       	
        request.setAttribute("liste",liste);
        
        if (!liste.isEmpty()) {
            	
        request.getRequestDispatcher(PathFile.DISPLAY_ANNONCE).forward(request, response);
        	
		}
        
        else {
        	 request.getRequestDispatcher(PathFile.EMPTY_ANNONCE).forward(request, response);
        }

	}
	
}