package fr.afpa.control;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Utilisateur;
import fr.afpa.model.GestionAnnonce;
import fr.afpa.technics.PathFile;

/**
 * Servlet implementation class SupprAnnonceAdmin
 */
public class SupprAnnonceAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public SupprAnnonceAdmin() {
        super();
       
    }

	
    /**
	 * Recupere l'identifiant de l'annonce, appelle la méthode de suppression et redirige vers la vue correspondante au résultat.
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		GestionAnnonce gest = new GestionAnnonce();
		
		int idAnnonce = Integer.parseInt(request.getParameter("idAnnonce"));
		
		if (gest.supprimerAnnonce(idAnnonce)) {
			
			request.getRequestDispatcher(PathFile.CONFIRM_SUPPR_ANNONCE).forward(request, response);
		}
		
		else {
			request.getRequestDispatcher(PathFile.ERROR_GEN).forward(request, response);
		}
	}

}
