package fr.afpa.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.model.GestionUser;
import fr.afpa.technics.PathFile;

/**
 * Servlet implementation class MuteUser
 */
public class MuteUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public MuteUser() {
        super();
        
    }

    /**
     * Recupere un idUser afin d'appeler la fonction de desactivation et de renvoyer vers la vue de confirmation ou d'erreur en fonction de la reponse BDD
     */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		GestionUser gest = new GestionUser();
		
		int idUser = Integer.parseInt(request.getParameter("idUser"));
		
		if (gest.desactiverUser(idUser)) {
			
			request.getRequestDispatcher(PathFile.CONFIRM_MUTE_USER).forward(request, response);
		}
		
		else {
			request.getRequestDispatcher(PathFile.ERROR_GEN).forward(request, response);
		}
	}

}
