package fr.afpa.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Utilisateur;
import fr.afpa.model.GestionUser;
import fr.afpa.technics.PathFile;

/**
 * Servlet implementation class DisplayAnnonceAdmin
 */
public class DisplayAnnonceAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayAnnonceAdmin() {
        super();
        // TODO Auto-generated constructor stub
    }


    /**
     * Recupere la liste des utilisateurs et renvoie à la vue afin d'afficher toutes les annonces
     */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		GestionUser gest = new GestionUser();
		
		List <Utilisateur> listeUsers = gest.recupererListeUsers();
		
		
		request.setAttribute("listeUsers",listeUsers);
        
        if (!listeUsers.isEmpty()) {
            	
        request.getRequestDispatcher(PathFile.DISPLAY_ANNONCE_ADMIN).forward(request, response);
        	
		}
        
        else {
        	 request.getRequestDispatcher(PathFile.EMPTY_ANNONCE).forward(request, response);
        }

	}
		
		
	}

