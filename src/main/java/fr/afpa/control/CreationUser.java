package fr.afpa.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Utilisateur;
import fr.afpa.model.GestionUser;
import fr.afpa.technics.PathFile;

/**
 * Servlet implementation class CreationUser
 */
public class CreationUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    public CreationUser() {
        super();
       
    }

    /**
     * Recupere les informations du formulaire de création d'utilisateur et renvoie à la vue correspondante (confirmation ou erreur).
     */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String nomLibrairie = request.getParameter("nomLibrairie");
		String voie = request.getParameter("voie");
		int codePostal = Integer.parseInt(request.getParameter("codePostal"));
		String ville = request.getParameter("ville");
		String email = request.getParameter("email");
		String tel = request.getParameter("tel");
		String login = request.getParameter("txtLogin");
		String mdp = request.getParameter("txtPassword");
		
		GestionUser gest = new GestionUser();
		
		
		
			if (gest.enregistrerUser(nom, prenom, nomLibrairie, voie, codePostal, ville, email, tel, login, mdp)) {
			
			// Renvoyez vers une page de confirmation d'enregistrement
		request.getRequestDispatcher(PathFile.CONFIRM_COMPTE).forward(request, response);
		}
		
		else {
			// Renvoyez vers une page d'erreur avec champs manquants 
			request.getRequestDispatcher(PathFile.ERROR_COMPTE).forward(request, response);
		}
	}

}
