package fr.afpa.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.technics.PathFile;

/**
 * Servlet implementation class RedirectionEspaceMembre
 */
public class RedirectionEspaceMembre extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RedirectionEspaceMembre() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
	 * Redirige vers l'espace membre
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher(PathFile.ESPACE_MEMBRE).forward(request, response);
	}

	

}
