package fr.afpa.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Utilisateur;
import fr.afpa.model.GestionAnnonce;
import fr.afpa.technics.PathFile;

/**
 * Servlet implementation class SaveModifAnnonce
 */
public class SaveModifAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SaveModifAnnonce() {
        super();
      
    }

	/**
	 * Recupere les champs de modifications d'annonce, appelle la méthode de modification et redirige vers la vue correspondante au résultat.
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String titre = request.getParameter("titre");
		String isbn = request.getParameter("isbn");
		String niveau = request.getParameter("niveau");
		String maisonEd = request.getParameter("maisonEd");
		String dateEd = request.getParameter("dateEd");
		double prix = Double.parseDouble(request.getParameter("prix"));
		int quantite = Integer.parseInt(request.getParameter("quantite"));
		double remise =  Double.parseDouble(request.getParameter("remise"));
		int id = Integer.parseInt(request.getParameter("idAnnonce"));
		
		
		Utilisateur user = (Utilisateur) request.getSession().getAttribute("user");
		
		GestionAnnonce gest = new GestionAnnonce();
		
		
		
		if (gest.modifierAnnonce(id,titre, isbn, niveau, maisonEd, dateEd, prix, quantite, remise, user)) {
			
			
			
			// Renvoyez vers une page de confirmation d'enregistrement
			request.getRequestDispatcher(PathFile.CONFIRM_MODIF_ANNONCE).forward(request, response);
			}
		
		else {
			// Renvoyez vers une page d'erreur 
			request.getRequestDispatcher(PathFile.ERROR_GEN).forward(request, response);
			}
	}

	}


