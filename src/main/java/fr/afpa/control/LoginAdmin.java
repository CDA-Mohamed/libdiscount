package fr.afpa.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.Administrateur;
import fr.afpa.beans.Utilisateur;
import fr.afpa.model.GestionAdmin;
import fr.afpa.model.GestionUser;
import fr.afpa.technics.PathFile;

/**
 * Servlet implementation class LoginAdmin
 */
public class LoginAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginAdmin() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    /**
	 * Recupere les champs de la page d'accueil, ouvre une session et redirige vers cette page en cas d'erreur
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String login = request.getParameter( "loginAdmin" );
        String password = request.getParameter( "passwordAdmin" );
        if ( login == null ) login = "";
        if ( password == null ) password = "";
        
        HttpSession session = request.getSession( true );
        session.setAttribute( "login", login );
        session.setAttribute( "password", password );
        
        request.getRequestDispatcher( PathFile.LOG_ADMIN ).forward( request, response );
    }

    /**
	 * Recupere les champs de la page d'accueil, ouvre une session et redirige vers l'espace admin.
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String login = request.getParameter( "loginAdmin" );
        String password = request.getParameter( "passwordAdmin" );

        HttpSession session = request.getSession( true );
        session.setAttribute( "password", password );
       
        
        GestionAdmin gest = new GestionAdmin();
        Administrateur admin = gest.checkAccess(login, password);
        	
        if (admin != null)
        					{
            session.setAttribute( "isConnected", true );
            session.setAttribute("user", login);
            request.getRequestDispatcher( PathFile.ESPACE_ADMIN).forward( request, response );
            
            
        } 
        
        else {
            session.setAttribute( "isConnected", false );
            request.getRequestDispatcher( PathFile.LOG_ADMIN ).forward( request, response );
            
        }
    }

}
