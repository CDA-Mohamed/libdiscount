package fr.afpa.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Annonce;
import fr.afpa.model.GestionAnnonce;
import fr.afpa.technics.PathFile;

/**
 * Servlet implementation class ModifAnnonce
 */
public class ModifAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public ModifAnnonce() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    /**
     * Recupere une annonce afin de l'envoyer vers la vue servant à la modifier
     */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int idAnnonce = Integer.parseInt(request.getParameter("annonce"));
		
		GestionAnnonce gest = new GestionAnnonce();
		
		Annonce annonce = gest.recupererAnnonce(idAnnonce);
		
		if (annonce != null) {
			request.setAttribute("annonceModif", annonce);
			request.getRequestDispatcher(PathFile.MODIF_ANNONCE).forward(request, response);
			
		}
		
		else {
			request.getRequestDispatcher(PathFile.ERROR_GEN).forward(request, response);
		}
		
	}

}
