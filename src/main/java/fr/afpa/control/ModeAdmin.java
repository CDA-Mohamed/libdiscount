package fr.afpa.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.technics.PathFile;

/**
 * Servlet implementation class ModeAdmin
 */
public class ModeAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModeAdmin() {
        super();
      
    }

	/**
	 * Sert a utiliser la servlet via la barre d'URL afin d'être redirigé vers le mode Administrateur
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher(PathFile.LOG_ADMIN).forward(request, response);
	}

}
