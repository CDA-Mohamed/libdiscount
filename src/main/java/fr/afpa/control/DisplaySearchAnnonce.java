package fr.afpa.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Utilisateur;
import fr.afpa.model.GestionAnnonce;
import fr.afpa.model.GestionUser;
import fr.afpa.technics.PathFile;

/**
 * Servlet implementation class DisplaySearchAnnonce
 */
public class DisplaySearchAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplaySearchAnnonce() {
        super();
     
    }

/**
 * Recupere en parametres le choix de filtre de recherche et affiche à la vue correspondante
 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String typeRecherche = request.getParameter("type");
		
		String choix = request.getParameter("choix");
		
		
		GestionUser gest = new GestionUser();
		
		List <Utilisateur> listeAllUsers = gest.recupererListeUsers();
		List <Annonce> listeType = new ArrayList();
		
		
		if (!listeAllUsers.isEmpty() && typeRecherche.equals("titre")) {
		
		for (Utilisateur user : listeAllUsers) {
			
			for (Annonce annonce : user.getListeAnnonces()) {
				
				if (annonce.getTitre().contains(choix) && annonce.isVisible()) {
					listeType.add(annonce);
				}
			}
		}
		request.setAttribute("listeAnnonces", listeType);
		
		request.getRequestDispatcher(PathFile.DISPLAY_SEARCH).forward(request, response);
	}
		
		else if (!listeAllUsers.isEmpty() && typeRecherche.equals("niveau")) {
		
			for (Utilisateur user : listeAllUsers) {
			
				for (Annonce annonce : user.getListeAnnonces()) {
					if (annonce.getNiveauScolaire().contains(choix) && annonce.isVisible()) {
					listeType.add(annonce);
					}
				}
			}
			request.setAttribute("listeAnnonces", listeType);
			
			request.getRequestDispatcher(PathFile.DISPLAY_SEARCH).forward(request, response);
		}
		
		
		else if (!listeAllUsers.isEmpty() && typeRecherche.equals("ville")) {
			
			for (Utilisateur user : listeAllUsers) {
			
				for (Annonce annonce : user.getListeAnnonces()) {
					if (user.getVille().contains(choix) && annonce.isVisible()) {
						listeType.add(annonce);
					}
				}
			}
			request.setAttribute("listeAnnonces", listeType);
			
			request.getRequestDispatcher(PathFile.DISPLAY_SEARCH).forward(request, response);
		}
		
		else if (listeAllUsers.isEmpty() || listeType.isEmpty()) {
			request.getRequestDispatcher(PathFile.EMPTY_SEARCH).forward(request, response);
		}
	}
}
