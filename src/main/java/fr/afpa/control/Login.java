package fr.afpa.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.beans.Utilisateur;
import fr.afpa.model.GestionUser;
import fr.afpa.technics.PathFile;

/**
 * Servlet implementation class Login
 */
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * Recupere les champs de la page d'accueil, ouvre une session et redirige vers cette page en cas d'erreur
	*/
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String login = request.getParameter( "txtLogin" );
        String password = request.getParameter( "txtPassword" );
        if ( login == null ) login = "";
        if ( password == null ) password = "";
        
        HttpSession session = request.getSession( true );
        session.setAttribute( "login", login );
        session.setAttribute( "password", password );
        
        request.getRequestDispatcher( "/auth.jsp" ).forward( request, response );
    }
    
    

	/**
	 * Recupere les champs de la page d'accueil, ouvre une session et redirige vers l'espace membre
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String login = request.getParameter( "txtLogin" );
        String password = request.getParameter( "txtPassword" );

        HttpSession session = request.getSession( true );
        session.setAttribute( "login", login );
        session.setAttribute( "password", password );

        
        GestionUser gest = new GestionUser();
        Utilisateur user = gest.checkAccess(login, password);
        	if (user != null && user.isActif())
        					{
            session.setAttribute( "isConnected", true );
            session.setAttribute("user", user);
            
            
            request.getRequestDispatcher( PathFile.ESPACE_MEMBRE).forward( request, response );
            
            
        } 
        
        else {
            session.setAttribute( "isConnected", false );
            request.getRequestDispatcher( "/auth.jsp" ).forward( request, response );
            
        }
    }

}
