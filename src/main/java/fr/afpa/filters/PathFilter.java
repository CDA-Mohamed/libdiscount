package fr.afpa.filters;

import java.io.IOException;
import java.net.HttpRetryException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



/**
 * Servlet Filter implementation class PathFilter
 */
public class PathFilter implements Filter {

    /**
     * Default constructor. 
     */
    public PathFilter() {
        // TODO Auto-generated constructor stub
    }

	
	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest) request;
		
		
		HttpSession session = req.getSession();
		
		String chemin = req.getServletPath();
		
		
		if (session.getAttribute("user") != null || chemin.equals("/ModeAdmin") || chemin.equals("/auth.jsp") || chemin.equals("/")
				|| chemin.equals("Login") || chemin.equals("LoginAdmin") || chemin.equals("/style.css")|| chemin.equals("/creation_compte.jsp")) {
		
		chain.doFilter(request, response);
		}
		
		else {
			req.getRequestDispatcher("/").forward(request, response);
		}
	}


	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}



}
