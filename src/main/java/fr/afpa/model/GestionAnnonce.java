package fr.afpa.model;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Utilisateur;
import fr.afpa.dao.GestionAnnonceDao;


public class GestionAnnonce {
	
	GestionAnnonceDao gest;
	
	
	/**
	 * Méthode permettant d'enregistrer une annonce et de la lier a un utilisateur
	 * @param titre
	 * @param niveau
	 * @param isbn
	 * @param dateEdition
	 * @param maisonEdition
	 * @param prix
	 * @param quantite
	 * @param remise
	 * @param user
	 * @return true si l'enregistrement est OK ou false s'il a rencontré un problème
	 */
	public boolean enregistrerAnnonce (String titre, String niveau, String isbn, String dateEdition, String maisonEdition, double prix, int quantite,double remise, Utilisateur user) {
		
		gest = new GestionAnnonceDao();
		
		List <Annonce> listeAnnonces = new ArrayList();
				
		Annonce annonce = new Annonce(titre, niveau, isbn, dateEdition, maisonEdition, prix, quantite, remise, user);	
		
		listeAnnonces.add(annonce);
		
		user.setListeAnnonces(listeAnnonces);		
		
		if (gest.enregistrerAnnonceDao(annonce)) {
			
			return true;
		}
		
		return false;
	}
	
	
	/**
	 * Récupère une liste d'annonces
	 * @param user : un utilisateur
	 * @return liste d'annonces de l'utilisateur
	 */
	public List<Annonce> recupererListeAnnonces (Utilisateur user) {
		
		gest = new GestionAnnonceDao();
		
		return gest.recupererListeAnnoncesDAO(user); 
	}
	

	/**
	 * Vérifie si une annonce a bien été supprimée
	 * @param idAnnonce : identifiant de l'annonce
	 * @return : true si supprimée ou false si erreur
	 */
	public boolean supprimerAnnonce(int idAnnonce) {
		gest = new GestionAnnonceDao();
		
		return gest.supprimerAnnonceDAO(idAnnonce); 
		
	}
	
	
	/** public List<Annonce> recupererListeAnnonces (String ville) {
		
		gest = new GestionAnnonceDao();
		
		return gest.recupererListeAnnoncesDAO(ville); 
	} **/
	

	
	/**
	 * Recupere une annonce par son identifiant
	 * @param idAnnonce : identifiant de l'annonce
	 * @return : une annonce ou un null
	 */
	public Annonce recupererAnnonce(int idAnnonce) {
		gest = new GestionAnnonceDao();
		
		return gest.recupAnnonceDAO(idAnnonce); 
	}

	
	/**
	 * Modifie une annonce d'un utilisateur
	 * @param id
	 * @param titre
	 * @param isbn
	 * @param niveau
	 * @param maisonEd
	 * @param dateEd
	 * @param prix
	 * @param quantite
	 * @param remise
	 * @param user
	 * @return : true si la modification a bien eu lieu et false dans le cas contraire
	 */
	public boolean modifierAnnonce(int id,String titre, String isbn, String niveau, String maisonEd, String dateEd,
			double prix, int quantite, double remise, Utilisateur user) {

		gest = new GestionAnnonceDao();
		
		List <Annonce> listeAnnonces = new ArrayList();
				
		Annonce annonce = new Annonce(id,titre, niveau, isbn, dateEd, maisonEd, prix, quantite, remise, user);	
		
		listeAnnonces.add(annonce);
		
		user.setListeAnnonces(listeAnnonces);
		
		
		
		if (gest.modifierAnnonceDao(annonce)) {
			
			return true;
		}
		
		return false;
	}


	/**
	 * Vérifie si une annonce a bien été supprimée
	 * @param idAnnonce : identifiant de l'annonce
	 * @return : true si supprimée ou false si erreur
	 */
	public boolean archiverAnnonce(int idAnnonce) {
		gest = new GestionAnnonceDao();
		
		return gest.archiverAnnonceDAO(idAnnonce); 
		
	}


}
