package fr.afpa.model;

import java.util.List;

import fr.afpa.beans.Utilisateur;
import fr.afpa.dao.GestionUserDao;

public class GestionUser {

	GestionUserDao gest;

	
	/**
	 * Enregistre un utilisateur en base de données
	 * @param nom
	 * @param prenom
	 * @param nomLibrairie
	 * @param adresse
	 * @param email
	 * @param tel
	 * @param login
	 * @param password
	 * @return : true si l'utilisateur a bien été enregistré, false si non.
	 */
	public boolean enregistrerUser(String nom, String prenom, String nomLibrairie, String voie, int codePostal, String ville, String email,
			String tel, String login, String password) {
		gest = new GestionUserDao();
		Utilisateur user = new Utilisateur(nom, prenom, nomLibrairie, voie, codePostal, ville, email, tel, login, password);

		if (gest.enregistrerUserDao(user)) {
			return true;
		}
		return false;
	}

	
	/**
	 * Verifie si l'utilisateur est inscrit en base de données
	 * @param login 
	 * @param password
	 * @return : l'utilisateur si celui-ci concorde avec les paramètres ou un null si non.
	 */
	public Utilisateur checkAccess(String login, String password) {
		gest = new GestionUserDao();
		return gest.checkAccessDAO(login, password);

	}

	
	/**
	 * Récupere une liste d'utilissateur
	 * @return : liste users
	 */
	public List <Utilisateur> recupererListeUsers() {
		gest = new GestionUserDao();
		return gest.recupListeUsersDao();
	}

	/**
	 * Desactive un utilisateur à l'aide de son ID
	 * @param idUser : identifiant de l'utilisateur
	 * @return : true si il a bien été desactivé ou false si non.
	 */
	public boolean desactiverUser(int idUser) {
		gest = new GestionUserDao();
		return gest.desactiverUsersDao(idUser);
	}

}