package fr.afpa.model;

import fr.afpa.beans.Administrateur;
import fr.afpa.beans.Utilisateur;
import fr.afpa.dao.GestionAdminDao;
import fr.afpa.dao.GestionUserDao;

public class GestionAdmin {
	GestionAdminDao gest;
	
	/**
	 * Verifie si l'admin est inscrit en base de données
	 * @param login 
	 * @param password
	 * @return : l'admin si celui-ci concorde avec les paramètres ou un null si non.
	 */
	public Administrateur checkAccess(String login, String password) {
		gest = new GestionAdminDao();
		return gest.checkAccessDAO(login, password);
	}

}
