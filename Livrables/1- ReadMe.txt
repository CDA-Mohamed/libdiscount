-------------------------------BIENVENUE DANS LE GUIDE READ'N SEARCH------------------------------------



INSTALLATION :

Veuillez récupérer le fichier war de l'application et le déployer dans un serveur TomCat Apache v9.

Créer une base de donnée avec les informations contenues dans le package src/main/ressources, à savoir :

<property name="hibernate.dialect">org.hibernate.dialect.PostgreSQLDialect</property>
<property name="hibernate.connection.driver_class">org.postgresql.Driver</property>
<property name="hibernate.connection.url">jdbc:postgresql://localhost:5432/postgres</property>
<property name="hibernate.connection.username">postgres</property>
<property name="hibernate.connection.password">979wak1i</property>

Ou modifiez ces lignes (12 a 16) et configurer votre BDD en fonction de vos choix.

Lancez le script joint aux livrables afin de créer la base de données Administrateur : vous pourrez-y accéder 
en ajoutant à votre URL principal "/ModeAdmin" et avec les identifiants de connexions suivants :
login : admin
mot de passe : admin

Notez toutefois que vous devrez d'abord vous connecter à un compte utilisateur afin de pouvoir accéder à
la page d'authentification admin.

Une fois que tout est en place, vous pouvez lancer l'application sur votre serveur !

FONCTIONNALITES :

Dans la page d'accueil, vous pouvez soit vous logger ou créer un compte.
Une fois connecté, vous pouvez créer une annonce, en rechercher une suivant 3 critères (ville, titre du
livre ou niveau scolaire), afficher vos propres annonces, les modifier ou les supprimer(archive).

En mode Admin, vous pouvez visualiser toute les annonces, en supprimer ou desactiver un utilisateur.
Vous avez le visuel des annonces visibles (non archivées) et du statut utilisateur (actif ou non).
Sachez qu'un utilisateur qui est desactivé ne pourra plus se connecter et ses annonces seront 
automatiquement archivées, usez de cette fonction avec prudence.

Nous espérons que cette application vous apportera entière satisfaction.

Malgré le soin que nous y avons apporté, si vous remarquez un dysfonctionnement ou avez des critiques
constructives ou des questions, n'hésitez pas à nous contacter à cette adresse :
mmeddah59@gmail.com

2020 - AFPA ROUBAIX - CDA MEDDAH MOHAMED